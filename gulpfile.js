/* eslint-disable */
var gulp = require('gulp');
var babel = require('gulp-babel');
var livereload = require('gulp-livereload');
var JS_FILES = ['./js/{,**/}*.js', '!./js/{,**/}*.min.js'];
var FILES_TO_RELOAD = [
  '*.html',
];

gulp.task('babel', function() {
  gulp
    .src(JS_FILES)
    .pipe(
      babel({
        presets: ['env'],
      })
    )
    .pipe(gulp.dest('dist'))
    .pipe(livereload());
});

gulp.task('minify-js', function() {
  gulp.src('dist/scripts.js').pipe(uglify()).pipe(gulp.dest('./dist'));
});

gulp.task('dev-watch', function() {
  livereload.listen();
  gulp.watch(JS_FILES, ['babel']);
  gulp.watch(FILES_TO_RELOAD).on('change', function(e) {
    console.log(e);
    livereload.reload();
  });
});

gulp.task('default', ['dev-watch']);