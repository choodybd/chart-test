const App = {
  chartOptions: {
    responsive: true,
    animation: {
      duration: 0,
    },
    scales: {
      xAxes: [
        {
          type: 'time',
          display: true,
        },
      ],
    },
  },
  median: (values, std) => {
    const lowMiddle = Math.floor((values.length - 1) / 2);
    const highMiddle = Math.ceil((values.length - 1) / 2);
    if (std) {
      values.sort((a, b) => a - b);
      const mean = (values[lowMiddle] + values[highMiddle]) / 2;
      return Math.sqrt(mean);
    }
    values.sort((a, b) => a.y - b.y);
    const mean = (values[lowMiddle].y + values[highMiddle].y) / 2;
    return mean;
  },
  getJSON: (url, callback) => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = () => {
      if (xhr.status === 200) {
        callback(xhr.responseText);
      } else {
        callback(null);
      }
    };
    xhr.send();
  },
  getBollingerBands: (n, k, data) => {
    const bands = [];
    for (let i = n - 1, len = data.length; i < len; i += 1) {
      const slice = data.slice(i + 1 - n, i);
      const mean = App.median(slice, false);
      const stdArray = slice.map(v => {
        let value = v.y - mean;
        value **= 2;
        return value;
      });
      const std = App.median(stdArray, true);
      bands.push({
        date: data[i].x,
        medium: mean.toFixed(2),
        low: (mean - k * std).toFixed(2),
        high: (mean + k * std).toFixed(2),
      });
    }
    return bands;
  },
  getRSI: (n, data) => {
    const rsi = [];
    const dataWithChange = [];
    let firstAG = 0;
    let firstAL = 0;
    for (let i = 1, len = data.length; i < len; i += 1) {
      dataWithChange.push({
        date: data[i].x,
        change: Number((data[i].y - data[i - 1].y).toFixed(2)),
      });
    }
    for (let i = n - 1, len = dataWithChange.length; i < len; i += 1) {
      if (!firstAG && !firstAL) {
        const slice = dataWithChange.slice(i + 1 - n, i);
        for (let j = 0, jlen = slice.length; j < jlen; j += 1) {
          if (slice[j].change > 0) {
            firstAG += slice[j].change;
          } else {
            firstAL += slice[j].change;
          }
        }
        const RS = firstAG / (firstAL * -1);
        const RSI = 100 - 100 / (1 + RS);
        rsi.push({
          x: dataWithChange[i].date,
          y: RSI.toFixed(2),
        });
      } else {
        if (dataWithChange[i].change > 0) {
          firstAG = (firstAG * (n - 1) + dataWithChange[i].change) / n;
          firstAL = firstAL * (n - 1) / n;
        } else {
          firstAG = firstAG * (n - 1) / n;
          firstAL = (firstAL * (n - 1) + dataWithChange[i].change) / n;
        }
        const RS = firstAG / (firstAL * -1);
        const RSI = 100 - 100 / (1 + RS);
        rsi.push({
          x: dataWithChange[i].date,
          y: RSI.toFixed(2),
        });
      }
    }
    return rsi;
  },
  renderChartBollingerBands: (id, dataSet) => {
    const n = 20;
    const k = 2;
    const data = dataSet.map(item => {
      return {
        x: new Date(item.date).toISOString().split('T')[0],
        y: item.last,
      };
    });
    const bands = App.getBollingerBands(n, k, data);
    const context = document.getElementById(id).getContext('2d');
    const chart = new Chart(context, {
      type: 'line',
      data: {
        datasets: [
          {
            label: `${id} - original value`,
            fill: false,
            data: data,
            borderWidth: 1,
            pointRadius: 1,
            pointHoverRadius: 1,
            backgroundColor: 'black',
            borderColor: 'rgba(0,0,0,0.2)',
          },
          {
            label: `${id} - high`,
            fill: false,
            data: bands.map(item => {
              return {
                x: new Date(item.date).toISOString().split('T')[0],
                y: item.high,
              };
            }),
            borderWidth: 1,
            pointRadius: 1,
            pointHoverRadius: 1,
            backgroundColor: '#1a5ecc',
            borderColor: '#1a5ecc',
          },
          {
            label: `${id} - low`,
            fill: '-1',
            data: bands.map(item => {
              return {
                x: new Date(item.date).toISOString().split('T')[0],
                y: item.low,
              };
            }),
            borderWidth: 1,
            pointRadius: 1,
            pointHoverRadius: 1,
            backgroundColor: 'rgba(47,138,224,0.07)',
            borderColor: '#2f8ae0',
          },
          {
            label: `${id} - medium`,
            fill: false,
            data: bands.map(item => {
              return {
                x: new Date(item.date).toISOString().split('T')[0],
                y: item.medium,
              };
            }),
            borderWidth: 1,
            pointRadius: 1,
            pointHoverRadius: 1,
            backgroundColor: '#f42929',
            borderColor: '#f42929',
          },
        ],
      },
      options: App.chartOptions,
    });
  },
  renderChartRsi: (id, dataSet) => {
    const n = 14;
    const data = dataSet.map(item => {
      return {
        x: new Date(item.date).toISOString().split('T')[0],
        y: item.last,
      };
    });
    const sets = App.getRSI(n, data);
    const context = document.getElementById(`${id}_ORG`).getContext('2d');
    const contextRsi = document.getElementById(`${id}_RSI`).getContext('2d');
    const chart = new Chart(context, {
      type: 'line',
      data: {
        datasets: [
          {
            label: `${id} - original value`,
            fill: false,
            data: data,
            borderWidth: 1,
            pointRadius: 1,
            pointHoverRadius: 1,
            backgroundColor: 'black',
            borderColor: 'rgba(0,0,0,0.2)',
          },
        ],
      },
      options: App.chartOptions,
    });
    const chartRsi = new Chart(contextRsi, {
      type: 'line',
      data: {
        datasets: [
          {
            label: `${id} - RSI`,
            fill: false,
            data: sets.map(item => {
              return {
                x: new Date(item.x).toISOString().split('T')[0],
                y: item.y,
              };
            }),
            borderWidth: 1,
            pointRadius: 1,
            pointHoverRadius: 1,
            backgroundColor: '#1a5ecc',
            borderColor: '#1a5ecc',
          },
        ],
      },
      options: App.chartOptions,
    });
  },
  init: () => {
    App.getJSON('data/Oslo_STL.json', data => {
      App.renderChartBollingerBands('STL', JSON.parse(data));
      App.renderChartRsi('STL', JSON.parse(data));
    });
    App.getJSON('data/Stockholm_ABB.json', data => {
      App.renderChartBollingerBands('ABB', JSON.parse(data));
      App.renderChartRsi('ABB', JSON.parse(data));
    });
  },
};

App.init();
