'use strict';

var App = {
  chartOptions: {
    responsive: true,
    animation: {
      duration: 0
    },
    scales: {
      xAxes: [{
        type: 'time',
        display: true
      }]
    }
  },
  median: function median(values, std) {
    var lowMiddle = Math.floor((values.length - 1) / 2);
    var highMiddle = Math.ceil((values.length - 1) / 2);
    if (std) {
      values.sort(function (a, b) {
        return a - b;
      });
      var _mean = (values[lowMiddle] + values[highMiddle]) / 2;
      return Math.sqrt(_mean);
    }
    values.sort(function (a, b) {
      return a.y - b.y;
    });
    var mean = (values[lowMiddle].y + values[highMiddle].y) / 2;
    return mean;
  },
  getJSON: function getJSON(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = function () {
      if (xhr.status === 200) {
        callback(xhr.responseText);
      } else {
        callback(null);
      }
    };
    xhr.send();
  },
  getBollingerBands: function getBollingerBands(n, k, data) {
    var bands = [];

    var _loop = function _loop(i, len) {
      var slice = data.slice(i + 1 - n, i);
      var mean = App.median(slice, false);
      var stdArray = slice.map(function (v) {
        var value = v.y - mean;
        value = Math.pow(value, 2);

        return value;
      });
      var std = App.median(stdArray, true);
      bands.push({
        date: data[i].x,
        medium: mean.toFixed(2),
        low: (mean - k * std).toFixed(2),
        high: (mean + k * std).toFixed(2)
      });
    };

    for (var i = n - 1, len = data.length; i < len; i += 1) {
      _loop(i, len);
    }
    return bands;
  },
  getRSI: function getRSI(n, data) {
    var rsi = [];
    var dataWithChange = [];
    var firstAG = 0;
    var firstAL = 0;
    for (var i = 1, len = data.length; i < len; i += 1) {
      dataWithChange.push({
        date: data[i].x,
        change: Number((data[i].y - data[i - 1].y).toFixed(2))
      });
    }
    for (var _i = n - 1, _len = dataWithChange.length; _i < _len; _i += 1) {
      if (!firstAG && !firstAL) {
        var slice = dataWithChange.slice(_i + 1 - n, _i);
        for (var j = 0, jlen = slice.length; j < jlen; j += 1) {
          if (slice[j].change > 0) {
            firstAG += slice[j].change;
          } else {
            firstAL += slice[j].change;
          }
        }
        var RS = firstAG / (firstAL * -1);
        var RSI = 100 - 100 / (1 + RS);
        rsi.push({
          x: dataWithChange[_i].date,
          y: RSI.toFixed(2)
        });
      } else {
        if (dataWithChange[_i].change > 0) {
          firstAG = (firstAG * (n - 1) + dataWithChange[_i].change) / n;
          firstAL = firstAL * (n - 1) / n;
        } else {
          firstAG = firstAG * (n - 1) / n;
          firstAL = (firstAL * (n - 1) + dataWithChange[_i].change) / n;
        }
        var _RS = firstAG / (firstAL * -1);
        var _RSI = 100 - 100 / (1 + _RS);
        rsi.push({
          x: dataWithChange[_i].date,
          y: _RSI.toFixed(2)
        });
      }
    }
    return rsi;
  },
  renderChartBollingerBands: function renderChartBollingerBands(id, dataSet) {
    var n = 20;
    var k = 2;
    var data = dataSet.map(function (item) {
      return {
        x: new Date(item.date).toISOString().split('T')[0],
        y: item.last
      };
    });
    var bands = App.getBollingerBands(n, k, data);
    var context = document.getElementById(id).getContext('2d');
    var chart = new Chart(context, {
      type: 'line',
      data: {
        datasets: [{
          label: id + ' - original value',
          fill: false,
          data: data,
          borderWidth: 1,
          pointRadius: 1,
          pointHoverRadius: 1,
          backgroundColor: 'black',
          borderColor: 'rgba(0,0,0,0.2)'
        }, {
          label: id + ' - high',
          fill: false,
          data: bands.map(function (item) {
            return {
              x: new Date(item.date).toISOString().split('T')[0],
              y: item.high
            };
          }),
          borderWidth: 1,
          pointRadius: 1,
          pointHoverRadius: 1,
          backgroundColor: '#1a5ecc',
          borderColor: '#1a5ecc'
        }, {
          label: id + ' - low',
          fill: '-1',
          data: bands.map(function (item) {
            return {
              x: new Date(item.date).toISOString().split('T')[0],
              y: item.low
            };
          }),
          borderWidth: 1,
          pointRadius: 1,
          pointHoverRadius: 1,
          backgroundColor: 'rgba(47,138,224,0.07)',
          borderColor: '#2f8ae0'
        }, {
          label: id + ' - medium',
          fill: false,
          data: bands.map(function (item) {
            return {
              x: new Date(item.date).toISOString().split('T')[0],
              y: item.medium
            };
          }),
          borderWidth: 1,
          pointRadius: 1,
          pointHoverRadius: 1,
          backgroundColor: '#f42929',
          borderColor: '#f42929'
        }]
      },
      options: App.chartOptions
    });
  },
  renderChartRsi: function renderChartRsi(id, dataSet) {
    var n = 14;
    var data = dataSet.map(function (item) {
      return {
        x: new Date(item.date).toISOString().split('T')[0],
        y: item.last
      };
    });
    var sets = App.getRSI(n, data);
    var context = document.getElementById(id + '_ORG').getContext('2d');
    var contextRsi = document.getElementById(id + '_RSI').getContext('2d');
    var chart = new Chart(context, {
      type: 'line',
      data: {
        datasets: [{
          label: id + ' - original value',
          fill: false,
          data: data,
          borderWidth: 1,
          pointRadius: 1,
          pointHoverRadius: 1,
          backgroundColor: 'black',
          borderColor: 'rgba(0,0,0,0.2)'
        }]
      },
      options: App.chartOptions
    });
    var chartRsi = new Chart(contextRsi, {
      type: 'line',
      data: {
        datasets: [{
          label: id + ' - RSI',
          fill: false,
          data: sets.map(function (item) {
            return {
              x: new Date(item.x).toISOString().split('T')[0],
              y: item.y
            };
          }),
          borderWidth: 1,
          pointRadius: 1,
          pointHoverRadius: 1,
          backgroundColor: '#1a5ecc',
          borderColor: '#1a5ecc'
        }]
      },
      options: App.chartOptions
    });
  },
  init: function init() {
    App.getJSON('data/Oslo_STL.json', function (data) {
      App.renderChartBollingerBands('STL', JSON.parse(data));
      App.renderChartRsi('STL', JSON.parse(data));
    });
    App.getJSON('data/Stockholm_ABB.json', function (data) {
      App.renderChartBollingerBands('ABB', JSON.parse(data));
      App.renderChartRsi('ABB', JSON.parse(data));
    });
  }
};

App.init();